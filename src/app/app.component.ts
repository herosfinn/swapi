import { Component } from '@angular/core';
import { Http, Response } from '@angular/http';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  private apiUrl = 'https://swapi.co/api/films/';
  data:any = {};
  dataSearch:any = {}
  idFilm: any;

  constructor(
    private http: Http
  ){
    this.dataFilm()
    this.getDataFilm()
  }

  getDataFilm(){
    return this.http.get(this.apiUrl)
    .pipe(map((res:Response) => res.json()))
  }

  dataFilm(){
    this.getDataFilm().subscribe(data=>{
      console.log(data)
      this.data = data
    })
  }
}
