How to run this project


1. install node.js and angular cli

2. clone this project (https://gitlab.com/herosfinn/swapi)

3. open your terminal and go to project directory

4. run npm install to get all node modules

5. after that run ng serve
